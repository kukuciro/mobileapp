import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:table_calendar/table_calendar.dart';

class notification extends StatefulWidget {
  const notification({super.key});

  @override
  State<notification> createState() => _notificationState();
}

class _notificationState extends State<notification> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Color(0xFF009cff));
    StatusBarControl.setStyle(StatusBarStyle.LIGHT_CONTENT);
  }

  List<bool> isReadList = [false, false, false, false, false];
  int totalReadCount = 0;

  List category = ["Workspace", "Form", "Info", "Form", "Info"];
  List info = [
    "Selamat Anda Tergabung dengan workspace Pilpres 2024",
    "Form Survey terkait elektabilitas pasion paslon 2024",
    "Form Survey terkait elektabilitas pasion pilpres 2024",
    "Form Survey terkait elektabilitas pasion paskon 2024",
    "Form Survey terkait elektabilitas pasion pilpres 2024"
  ];
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(color: Color(0xFF009cff)),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.arrow_back_ios_new_rounded,
                          color: Colors.white,
                        ),
                        Text(
                          'Notifikasi',
                          style: TextStyle(
                              fontFamily: 'calibri',
                              fontSize: 24,
                              color: Colors.white,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 7,
              child: Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Terbaru',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                          Visibility(
                            visible: totalReadCount > 0,
                            child: Text(
                              'Tandai Sudah di Baca ($totalReadCount)',
                              style: TextStyle(
                                color: Color(0xFF009cff),
                                fontFamily: 'calibri',
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        width: double.infinity,
                        child: ListView.builder(
                          itemCount: 5,
                          itemBuilder: (context, index) {
                            bool isRead = isReadList[index];
                            if (index == 2) {
                              return GestureDetector(
                                onLongPress: () {
                                  setState(() {
                                    if (isRead) {
                                      // Jika sudah dibaca, kurangi jumlah dan ubah warna
                                      totalReadCount--;
                                      isReadList[index] = false;
                                    } else {
                                      // Jika belum dibaca, tambah jumlah dan ubah warna
                                      totalReadCount++;
                                      isReadList[index] = true;
                                    } // Tambah jumlah pembacaan
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: isRead ? Color.fromARGB(255, 226, 242, 255): Colors.white,
                                  ),
                                  height:
                                      MediaQuery.of(context).size.height * 0.34,
                                  child: Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            CircleAvatar(
                                              radius: 15,
                                              backgroundColor:
                                                  Colors.transparent,
                                              backgroundImage: AssetImage(
                                                  'assets/images/iconProfile.png'),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Pilpres 2024',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontFamily: 'calibri',
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    '2 Jam Lalu',
                                                    style: TextStyle(
                                                        fontFamily: 'calibri',
                                                        color: Colors.grey),
                                                  )
                                                ],
                                              ),
                                              Text(
                                                category[index],
                                                style: TextStyle(
                                                    fontFamily: 'calibri',
                                                    fontSize: 16,
                                                    color: Color(0xFF009cff)),
                                              ),
                                              Text(
                                                info[index],
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'calibri'),
                                              ),
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                child: Image(
                                                    image: AssetImage(
                                                        'assets/images/pilpres.png')),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            } else {
                              return GestureDetector(
                                onLongPress: () {
                                  setState(() {
                                    if (isRead) {
                                      // Jika sudah dibaca, kurangi jumlah dan ubah warna
                                      totalReadCount--;
                                      isReadList[index] = false;
                                    } else {
                                      // Jika belum dibaca, tambah jumlah dan ubah warna
                                      totalReadCount++;
                                      isReadList[index] = true;
                                    } // Tambah jumlah pembacaan
                                  });
                                },
                                child: Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.12,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: isRead ? Color.fromARGB(255, 226, 242, 255): Colors.white ,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            CircleAvatar(
                                              radius: 15,
                                              backgroundColor:
                                                  Colors.transparent,
                                              backgroundImage: AssetImage(
                                                  'assets/images/iconProfile.png'),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Pilpres 2024',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontFamily: 'calibri',
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    '2 Jam Lalu',
                                                    style: TextStyle(
                                                        fontFamily: 'calibri',
                                                        color: Colors.grey),
                                                  )
                                                ],
                                              ),
                                              Text(
                                                category[index],
                                                style: TextStyle(
                                                    fontFamily: 'calibri',
                                                    fontSize: 16,
                                                    color: Color(0xFF009cff)),
                                              ),
                                              Text(
                                                info[index],
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'calibri'),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:table_calendar/table_calendar.dart';

class mainProfile extends StatefulWidget {
  const mainProfile({super.key});

  @override
  State<mainProfile> createState() => _mainProfileState();
}

class _mainProfileState extends State<mainProfile> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Color(0xFF009cff));
    StatusBarControl.setStyle(StatusBarStyle.LIGHT_CONTENT);
  }

  List image = [
    'assets/images/covid.png',
    'assets/images/covid2.png',
    'assets/images/covid2.png'
  ];

  List<Color> colors = [
    Colors.blue,
    Colors.amber,
    Colors.grey,
    Colors.green,
    Colors.yellow,
    Colors.pink
  ];

  List<String> Klien = [
    "Klien A",
    "Klien B",
    "Klien C",
    "Klien D",
    "Klien E",
    "Klien F",
  ];

  Widget buildListItem(String text, Color Warna) {
    return Column(
      children: [
        CircleAvatar(
          radius: 30,
          backgroundColor: Warna,
        ),
        Text('$text')
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.35,
                decoration: BoxDecoration(
                    color: Color(0xFF009cff),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(
                            MediaQuery.of(context).size.width * 0.15),
                        bottomRight: Radius.circular(
                            MediaQuery.of(context).size.width * 0.15))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.arrow_back_ios_new_rounded,
                            color: Colors.white,
                          ),
                          Icon(
                            Icons.logout_rounded,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: AssetImage('assets/images/iconProfile.png')),
                      ),
                      height: MediaQuery.of(context).size.height * 0.12,
                      width: MediaQuery.of(context).size.width * 0.21,
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Icon(
                          Icons.edit_outlined,
                          size: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Andri Yulianto Rosadi',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'calibri',
                                fontSize: 20)),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.height * 0.1),
                        child: Row(
                          children: [
                            Expanded(
                                child: Row(
                              children: [
                                Icon(
                                  Icons.email_outlined,
                                  color: Colors.white,
                                ),
                                Text(
                                  'Test@gmail.com',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            )),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.03,
                                color: Colors.white,
                                width: 1.5,
                              ),
                            ),
                            Expanded(
                                child: Row(
                              children: [
                                Icon(
                                  Icons.phone_outlined,
                                  color: Colors.white,
                                ),
                                Text(
                                  '0210230223',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ))
                          ],
                        )),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.chrome_reader_mode_outlined,color: Colors.white,),
                        SizedBox(width: 5,),
                        Text(
                          '0210230223',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        )
                      ],
                    )
                  ],
                ), // Warna biru di atas
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  child: Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Workspace Saya',
                                style: TextStyle(
                                    fontFamily: 'calibri',
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'Undangan',
                                style: TextStyle(
                                    fontFamily: 'calibri',
                                    fontSize: 16,
                                    color: Color(0xFF009cff),
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          )),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 6,
                          itemExtent: MediaQuery.of(context).size.width * 0.2,
                          itemBuilder: (context, index) {
                            return buildListItem(
                                '${Klien[index]}', colors[index]);
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            'Form Saya',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: double.infinity,
                          child: ListView.builder(
                            itemCount: 3,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white,
                                      border: Border.all(
                                          color: Colors.grey.shade300)),
                                  width: double.infinity,
                                  height:
                                      MediaQuery.of(context).size.height * 0.12,
                                  child: Stack(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(15.0),
                                        child: Row(
                                          children: [
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.15,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.08,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      image: DecorationImage(
                                                          image: AssetImage(
                                                              image[index]))),
                                                  child: Icon(
                                                    Icons.library_books,
                                                    color: Colors.grey,
                                                    size: 15,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Andri',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontFamily: 'calibri',
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    "Lorem Ipsum is simply dummy and scrambled its........",
                                                    style: TextStyle(
                                                        color: Colors.grey,
                                                        fontFamily: 'calibri'),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "23/03/22 16:36",
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'calibri',
                                                            color: Colors.grey),
                                                      ),
                                                      Text(
                                                        'Klien A',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'calibri',
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            color: Color(
                                                                0xFF009cff)),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Align(
                                          alignment: Alignment.topRight,
                                          child: Container(
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color(0xFF00c796)),
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.022,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.022,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ))
            ],
          )),
    );
  }
}

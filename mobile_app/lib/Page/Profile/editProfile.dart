import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class editProfile extends StatefulWidget {
  const editProfile({super.key});

  @override
  State<editProfile> createState() => _editProfileState();
}

class _editProfileState extends State<editProfile> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
  }

  String? selectedForm; // Variabel untuk menyimpan nilai pilihan

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 1,
          centerTitle: true,
          title: Text(
            'Account Information',
            style: TextStyle(
                fontFamily: 'calibri',
                fontSize: 22,
                fontWeight: FontWeight.w500,
                color: Colors.black),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
              splashRadius: 20,
              onPressed: () {},
              icon: Icon(
                Icons.arrow_back_ios_new_rounded,
                color: Colors.black,
              )),
        ),
        body: Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: AssetImage('assets/images/profileMale.png')),
                      ),
                      height: MediaQuery.of(context).size.height * 0.12,
                      width: MediaQuery.of(context).size.width * 0.21,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              fixedSize: Size(
                                  MediaQuery.of(context).size.width * 0.3,
                                  MediaQuery.of(context).size.height * 0.04),
                              backgroundColor: Color(0xFF009cff)),
                          onPressed: () {},
                          child: Text('Edit Photo')),
                    )
                  ],
                ),
                Expanded(
                    child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: RichText(
                          text: TextSpan(
                            text: 'Name',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontFamily: 'calibri',
                              color: Colors
                                  .black, // Warna teks sebelum tanda bintang
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: '*',
                                style: TextStyle(
                                  fontFamily: 'calibri',
                                  color: Colors
                                      .red, // Warna teks setelah tanda bintang
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        hintText: 'Name',
                        contentPadding: EdgeInsets.all(10),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: RichText(
                          text: TextSpan(
                            text: 'Email',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontFamily: 'calibri',
                              color: Colors
                                  .black, // Warna teks sebelum tanda bintang
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: '*',
                                style: TextStyle(
                                  fontFamily: 'calibri',
                                  color: Colors
                                      .red, // Warna teks setelah tanda bintang
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        hintText: 'Email',
                        hintStyle: TextStyle(
                            color: Colors.grey, fontFamily: 'calibri'),
                        contentPadding: EdgeInsets.all(10),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: RichText(
                          text: TextSpan(
                            text: 'NIK',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontFamily: 'calibri',
                              color: Colors
                                  .black, // Warna teks sebelum tanda bintang
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: '*',
                                style: TextStyle(
                                  fontFamily: 'calibri',
                                  color: Colors
                                      .red, // Warna teks setelah tanda bintang
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    TextField(
                      decoration: InputDecoration(
                        hintText: 'NIK',
                        hintStyle: TextStyle(
                            color: Colors.grey, fontFamily: 'calibri'),
                        contentPadding: EdgeInsets.all(10),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text('Gender')),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: Colors.grey.shade300)),
                      width: MediaQuery.of(context).size.width * 0.95,
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          hint: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0), // Atur jarak di sini
                            child: Text(
                              'Pilih Gender',
                              style: TextStyle(color: Colors.grey.shade400),
                            ),
                          ),
                          elevation: 1,
                          value: selectedForm,
                          borderRadius: BorderRadius.circular(5),
                          icon: Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: FaIcon(
                              FontAwesomeIcons.chevronDown,
                              size: 14,
                              color: Colors.grey.shade700,
                            ),
                          ),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.black),
                          items: [
                            DropdownMenuItem<String>(
                              value: 'Male',
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text('Male'),
                              ),
                            ),
                            DropdownMenuItem<String>(
                              value: 'Female',
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text('Female'),
                              ),
                            ),
                          ],
                          onChanged: (value) {
                            setState(() {
                              selectedForm = value!;
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text('Address')),
                    ),
                    TextField(
                      maxLines: 4,
                      decoration: InputDecoration(
                        hintText: 'Address',
                        hintStyle: TextStyle(
                            color: Colors.grey, fontFamily: 'calibri'),
                        contentPadding: EdgeInsets.all(10),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey.shade300)),
                      ),
                    )
                  ],
                )),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        backgroundColor: Color(0xFF009cff),
                        fixedSize: Size(MediaQuery.of(context).size.width,
                            MediaQuery.of(context).size.height * 0.05)),
                    onPressed: () {},
                    child: Text('Save & Changes'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

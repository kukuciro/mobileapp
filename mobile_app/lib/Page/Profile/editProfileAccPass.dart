import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class editForm extends StatefulWidget {
  const editForm({super.key});

  @override
  State<editForm> createState() => _editFormState();
}

class _editFormState extends State<editForm> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
  }

  String? selectedForm; // Variabel untuk menyimpan nilai pilihan

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text(
            'Edit Profile',
            style: TextStyle(
                fontFamily: 'calibri', fontSize: 22,fontWeight: FontWeight.w500, color: Colors.black),
          ),
          backgroundColor: Colors.white,
          leading: IconButton(
              splashRadius: 20,
              onPressed: () {},
              icon: Icon(
                Icons.arrow_back_ios_new_rounded,
                color: Colors.black,
              )),
        ),
        body: Container(
            width: double.infinity,
            child: ListView(
              children: [
                Divider(
                  thickness: 1,
                  height: 1,
                ),
                ListTile(
                  leading: Icon(Icons.group_outlined,
                      color: Colors.black,
                      size: MediaQuery.of(context).size.height * 0.03),
                  minLeadingWidth: 10,
                  title: Text('Account Information'),
                  trailing: Icon(Icons.chevron_right_rounded),
                  onTap: () {
                    // Aksi yang akan dijalankan saat ListTile diklik
                  },
                ),
                Divider(
                  thickness: 1,
                  height: 0,
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/images/password.png'),
                    width: MediaQuery.of(context).size.width * 0.05,
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  minLeadingWidth: 10,
                  title: Text('Password'),
                  trailing: Icon(Icons.chevron_right_rounded),
                  onTap: () {
                    // Aksi yang akan dijalankan saat ListTile diklik
                  },
                ),
                Divider(
                  thickness: 1,
                  height: 0,
                ),
              ],
            )),
      ),
    );
  }
}

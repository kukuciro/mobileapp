import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';

class gpsNotActive extends StatefulWidget {
  const gpsNotActive({super.key});

  @override
  State<gpsNotActive> createState() => _gpsNotActiveState();
}

class _gpsNotActiveState extends State<gpsNotActive> {
  @override
  void initState() {
    // TODO: implement initState
    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
        backgroundColor: Colors.white,

      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/gpsNoActive.png',
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.5,
            ),
            Text(
              'GPS Tidak Aktif',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.22),
              child: Text(
                'Whoops....Sepertinya GPS Anda tidak aktif, ke pengaturan untuk mengaktifkan',
                textAlign: TextAlign.center,
                style: TextStyle(height: 1.5),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * 0.035,
                horizontal: MediaQuery.of(context).size.width * 0.07,
              ),
              child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Color(0xFF009cff)),
                      minimumSize:
                          MaterialStatePropertyAll(Size(double.infinity, 40))),
                  onPressed: () {},
                  child: Text(
                    'Pergi ke Pengaturan',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  )),
            )
          ],
        ),
      ),
    ));
  }
}

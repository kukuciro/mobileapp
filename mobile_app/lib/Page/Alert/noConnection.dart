import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';

class noConnection extends StatefulWidget {
  const noConnection({super.key});

  @override
  State<noConnection> createState() => _noConnectionState();
}

class _noConnectionState extends State<noConnection> {
  @override
  void initState() {
    // TODO: implement initState
    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/noConnection.png',
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.5,
            ),
            Text(
              'Tidak ada koneksi internet',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.16),
              child: Text(
                'Whoops....Mohon periksa kembali koneksi internet anda dan coba lagi',
                textAlign: TextAlign.center,
                style: TextStyle(height: 1.5),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * 0.035,
                horizontal: MediaQuery.of(context).size.width * 0.07,
              ),
              child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Color(0xFF009cff)),
                      minimumSize:
                          MaterialStatePropertyAll(Size(double.infinity, 40))),
                  onPressed: () {},
                  child: Text(
                    'Coba Lagi',
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
                  )),
            )
          ],
        ),
      ),
    ));
  }
}

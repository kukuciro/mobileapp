import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';

class locationNotFound extends StatefulWidget {
  const locationNotFound({super.key});

  @override
  State<locationNotFound> createState() => _locationNotFoundState();
}

class _locationNotFoundState extends State<locationNotFound> {
   @override
  void initState() {
    // TODO: implement initState
    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
        backgroundColor: Colors.white,

      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/locationNotFound.png',
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.5,
            ),
            Text(
              'Lokasi Anda Tidak Terdeteksi',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.22),
              child: Text(
                'Sepertinya Lokasi Anda tidak Benar, sesuaikan lokasi Anda',
                textAlign: TextAlign.center,
                style: TextStyle(height: 1.5),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height * 0.035,
                horizontal: MediaQuery.of(context).size.width * 0.07,
              ),
              child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Color(0xFF009cff)),
                      minimumSize:
                          MaterialStatePropertyAll(Size(double.infinity, 40))),
                  onPressed: () {},
                  child: Text(
                    'Kembali',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  )),
            )
          ],
        ),
      ),
    ));
  }
}

import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class pilihForm extends StatefulWidget {
  const pilihForm({super.key});

  @override
  State<pilihForm> createState() => _pilihFormState();
}

class _pilihFormState extends State<pilihForm> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
  }

  String? selectedForm; // Variabel untuk menyimpan nilai pilihan

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: IconButton(
            splashRadius: 20,
              onPressed: () {},
              icon: Icon(
                Icons.arrow_back_ios_new_rounded,
                color: Colors.black,
              )),
        ),
        body: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: AssetImage('assets/images/pilihForm.png'),
                height: MediaQuery.of(context).size.height * 0.35,
                width: MediaQuery.of(context).size.width * 0.7,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.2),
                child: Column(
                  children: [
                    Text(
                      'Pilih Form',
                      style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'calibri',
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint.',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.grey.shade600),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: Colors.grey.shade300)),
                      width: MediaQuery.of(context).size.width * 0.85,
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8.0), // Atur jarak di sini
                        child: Text(
                          'Pilih Form',
                          style: TextStyle(color: Colors.grey.shade400),
                        ),
                      ),
                      elevation: 1,
                      isExpanded: true,
                      value: selectedForm,
                      borderRadius: BorderRadius.circular(5),
                      icon: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: FaIcon(
                          FontAwesomeIcons.chevronDown,
                          size: 20,
                          color: Colors.grey.shade300,
                        ),
                      ),
                      items: [
                        DropdownMenuItem<String>(
                          value: 'Elektabilitas Paslon Kaltim',
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Elektabilitas Paslon Kaltim'),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: 'Laporan Kegiatan Marketing',
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Laporan Kegiatan Marketing'),
                          ),
                        ),
                      ],
                      onChanged: (value) {
                        setState(() {
                          selectedForm = value!;
                        });
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:status_bar_control/status_bar_control.dart';

class kirimForm extends StatefulWidget {
  const kirimForm({super.key});

  @override
  State<kirimForm> createState() => _kirimFormState();
}

class _kirimFormState extends State<kirimForm> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
  }

  String? selectedForm; // Variabel untuk menyimpan nilai pilihan

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<String>(
                      hint: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8.0), // Atur jarak di sini
                        child: Text(
                          'Pilih Form',
                          style: TextStyle(color: Colors.grey.shade400),
                        ),
                      ),
                      elevation: 1,
                      value: selectedForm,
                      borderRadius: BorderRadius.circular(5),
                      icon: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: FaIcon(
                          FontAwesomeIcons.chevronDown,
                          size: 14,
                          color: Colors.grey.shade700,
                        ),
                      ),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                      items: [
                        DropdownMenuItem<String>(
                          value: 'Elektabilitas Paslon Kaltim',
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Elektabilitas Paslon Kaltim'),
                          ),
                        ),
                        DropdownMenuItem<String>(
                          value: 'Laporan Kegiatan Marketing',
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Laporan Kegiatan Marketing'),
                          ),
                        ),
                      ],
                      onChanged: (value) {
                        setState(() {
                          selectedForm = value!;
                        });
                      },
                    ),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.75,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: Colors.grey.shade300)),
                    child: Padding(
                      padding: EdgeInsets.all(20),
                      child: TextField(
                        decoration: InputDecoration(border: InputBorder.none),
                        maxLines: null,
                        maxLength: null,
                        autofocus: true,
                        cursorWidth: 1,
                        cursorColor: Colors.black,
                        style: TextStyle(fontSize: 18),
                      ),
                    )),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                              side: BorderSide(color: Color(0xFF009cff)),
                              fixedSize: Size(
                                  MediaQuery.of(context).size.width ,
                                  MediaQuery.of(context).size.height * 0.05)),
                          onPressed: () {},
                          child: Text(
                            'Batal',
                            style: TextStyle(fontFamily: 'calibri',
                                color: Color(0xFF009cff),
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              fixedSize: Size(
                                  MediaQuery.of(context).size.width ,
                                  MediaQuery.of(context).size.height * 0.05)),
                          onPressed: () {},
                          child: Text(
                            'Kirim',
                            style: TextStyle(
                                fontFamily: 'calibri', fontWeight: FontWeight.bold),
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

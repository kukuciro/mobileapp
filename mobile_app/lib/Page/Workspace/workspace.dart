import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:table_calendar/table_calendar.dart';

class workspace extends StatefulWidget {
  const workspace({super.key});

  @override
  State<workspace> createState() => _workspaceState();
}

class _workspaceState extends State<workspace> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Color(0xFF009cff));
    StatusBarControl.setStyle(StatusBarStyle.LIGHT_CONTENT);
  }

  List image = ['assets/images/covid.png', 'assets/images/covid2.png'];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.35,
                decoration: BoxDecoration(
                    color: Color(0xFF009cff),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(
                            MediaQuery.of(context).size.width * 0.15),
                        bottomRight: Radius.circular(
                            MediaQuery.of(context).size.width * 0.15))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Icon(
                                    Icons.arrow_back_ios_new_rounded,
                                    color: Colors.white,
                                  ))),
                          Text(
                            'Workspace',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'calibri',
                                fontSize: 25),
                          ),
                          Expanded(child: SizedBox()),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(width: 2, color: Colors.white),
                        image: DecorationImage(
                            image: AssetImage('assets/images/profileMale.png')),
                      ),
                      height: MediaQuery.of(context).size.height * 0.12,
                      width: MediaQuery.of(context).size.width * 0.21,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 15),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text('Klien B',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'calibri',
                                fontSize: 20)),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  MediaQuery.of(context).size.height * 0.1),
                          child: Text(
                            textAlign: TextAlign.center,
                            'Ini berisikan deskripsi yang berkaitan dengan pembuatan workspace A',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w300),
                          )),
                    ),
                  ],
                ), // Warna biru di atas
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width * 0.08,
                                  height:
                                      MediaQuery.of(context).size.width * 0.08,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Color(0xFF009cff)),
                                  child: Icon(
                                    Icons.person,
                                    color: Colors.white,
                                    size: 22,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text('Pembuat')
                              ],
                            ),
                            Expanded(
                                child: Align(
                              alignment: Alignment.centerRight,
                              child: Text('Subroto(Suroto@ebddesk)'),
                            ))
                          ],
                        ),
                      ),
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width * 0.08,
                                  height:
                                      MediaQuery.of(context).size.width * 0.08,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Color(0xFF00b4b5)),
                                  child: Icon(
                                    Icons.calendar_today,
                                    color: Colors.white,
                                    size: 22,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text('Tanggal Dibuat')
                              ],
                            ),
                            Expanded(
                                child: Align(
                              alignment: Alignment.centerRight,
                              child: Text('12 March 2022'),
                            ))
                          ],
                        ),
                      ),
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.08,
                                    height:
                                        MediaQuery.of(context).size.width * 0.08,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Color(0xFFffb718)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/hotspot.png'),
                                        color: Colors.white,
                                      ),
                                    )),
                                SizedBox(
                                  width: 10,
                                ),
                                Text('Status')
                              ],
                            ),
                            Expanded(
                                child: Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                'Aktif',
                                style: TextStyle(color: Color(0xFF00c796)),
                              ),
                            ))
                          ],
                        ),
                      ),
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            'Formulir',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: double.infinity,
                          child: ListView.builder(
                            itemCount: 2,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white,
                                      border: Border.all(
                                          color: Colors.grey.shade300)),
                                  width: double.infinity,
                                  height:
                                      MediaQuery.of(context).size.height * 0.12,
                                  child: Stack(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(15.0),
                                        child: Row(
                                          children: [
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.15,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.08,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      image: DecorationImage(
                                                          image: AssetImage(
                                                              image[index]))),
                                                  child: Icon(
                                                    Icons.library_books,
                                                    color: Colors.grey,
                                                    size: 15,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Andri',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.black,
                                                        fontFamily: 'calibri',
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    "Lorem Ipsum is simply dummy and scrambled its........",
                                                    style: TextStyle(
                                                        color: Colors.grey,
                                                        fontFamily: 'calibri'),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "23/03/22 16:36",
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'calibri',
                                                            color: Colors.grey),
                                                      ),
                                                      Text(
                                                        'Klien A',
                                                        style: TextStyle(
                                                            fontFamily:
                                                                'calibri',
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            color: Color(
                                                                0xFF009cff)),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Align(
                                          alignment: Alignment.topRight,
                                          child: Container(
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Color(0xFF00c796)),
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.022,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.022,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ))
            ],
          )),
    );
  }
}

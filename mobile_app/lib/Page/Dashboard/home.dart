import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';

class home extends StatefulWidget {
  const home({super.key});

  @override
  State<home> createState() => _homeState();
}

class _homeState extends State<home> {
  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Color(0xFF009cff));
    StatusBarControl.setStyle(StatusBarStyle.LIGHT_CONTENT);
    StatusBarControl.setFullscreen(true);
  }

  List gambar = ['assets/images/allReport.png', 'assets/images/draft.png'];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              'assets/images/background.png',
              fit: BoxFit.fill,
            ),
          ),
          Container(
            width: double.infinity,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    right: MediaQuery.of(context).size.width * 0.04,
                    left: MediaQuery.of(context).size.width * 0.04,
                    top: MediaQuery.of(context).size.height * 0.06,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.035,
                          width: MediaQuery.of(context).size.width * 0.16,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white.withOpacity(0.3)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Icon(
                                Icons.filter_alt,
                                color: Colors.white,
                                size: 20,
                              ),
                              Text(
                                'Filter',
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            onPressed: () {},
                            icon: Image(
                              image:
                                  AssetImage('assets/images/notification.png'),
                            ),
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Image(
                              image:
                                  AssetImage('assets/images/iconProfile.png'),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Row(
                    children: [
                      Text(
                        'Hi, Michael',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'calibri',
                            fontSize: 30),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: TextField(
                    maxLines: 1,
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      hintText: 'Search',
                      hintStyle: TextStyle(
                        color: Colors.grey,
                        fontFamily: 'calibri',
                      ),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.grey,
                      ),
                      fillColor: Colors.white,
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300)),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300)),
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.14,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                    itemCount: 2,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Card(
                          color: Colors.white,
                          elevation: 2, // Tingkat elevasi card
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(5), // Bentuk card
                          ),
                          child: Container(
                            height: MediaQuery.of(context).size.height *
                                0.15, // Tinggi card
                            width: MediaQuery.of(context).size.width *
                                0.55, // Lebar card, sesuaikan dengan kebutuhan
                            padding:
                                EdgeInsets.all(8.0), // Padding di dalam card
                            child: Row(
                              children: [
                                Image.asset(
                                  gambar[index],
                                  height:
                                      65, // Tinggi gambar, sesuaikan dengan kebutuhan
                                ),
                                SizedBox(width: 8.0),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '45',
                                      style: TextStyle(
                                          fontFamily: 'calibri',
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Laporan Terakhir',
                                      style: TextStyle(
                                          fontSize: 16, fontFamily: 'calibri'),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Laporan Terakhir',
                        style: TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 18),
                      ),
                    ],
                  ),
                ),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Container(
                    width: double.infinity,
                    child: ListView.builder(
                      itemCount: 6,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey.shade300)),
                            width: double.infinity,
                            height: MediaQuery.of(context).size.height * 0.12,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Row(
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.05,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.065,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                            border: Border.all(
                                                color: Colors.grey.shade300)),
                                        child: Icon(
                                          Icons.library_books,
                                          color: Colors.grey,
                                          size: 15,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              'Andri',
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.black,
                                                  fontFamily: 'calibri',
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              '2021103923',
                                              style: TextStyle(
                                                  fontFamily: 'calibri',
                                                  color: Color(0xFF009cff)),
                                            )
                                          ],
                                        ),
                                        Text(
                                          "Lorem Ipsum is simply dummy and scrambled its........",
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontFamily: 'calibri'),
                                        ),
                                        Text(
                                          "23/03/22 16:36",
                                          style: TextStyle(
                                              fontFamily: 'calibri',
                                              color: Colors.grey),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ))
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:table_calendar/table_calendar.dart';

class searchResult extends StatefulWidget {
  const searchResult({super.key});

  @override
  State<searchResult> createState() => _searchResultState();
}

class _searchResultState extends State<searchResult> {
  DateTime selectedDate = DateTime.now();
  TextEditingController tanggalAwal = TextEditingController();
  TextEditingController tanggalSampai = TextEditingController();

  Future<void> selectTanggalAwal(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        tanggalAwal.text = "${selectedDate.toLocal()}".split(' ')[0];
      });
    }
  }

  Future<void> selectTanggalSampai(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        tanggalSampai.text = "${selectedDate.toLocal()}".split(' ')[0];
      });
    }
  }

  @override
  void initState() {
    super.initState();
    StatusBarControl.setColor(Color(0xFF009cff));
    StatusBarControl.setStyle(StatusBarStyle.LIGHT_CONTENT);
  }

  bool isChecked = false;

  void show() {
    List<String> categories = [
      'Elektabilitas Paslon Kaltim',
      'Laporan Covid',
      'Laporan Kegiatan Marketing'
    ];
    List<String> categoriesDay = [
      'Hari Ini',
      'Minggu Ini',
      'Bulan Ini',
      'Tahun Ini'
    ];
    int selectedCategoryIndex = -1;
    int selectedCategoryDayIndex = -1;

    showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, setState) {
          return Padding(
            padding: EdgeInsets.only(
                top: 10, bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              height: MediaQuery.of(context).size.height *
                  0.7, // Atur tinggi sesuai kebutuhan Anda

              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30))),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.26,
                      height: MediaQuery.of(context).size.height * 0.005,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.grey.shade300),
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey.shade100.withOpacity(0.8),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.08,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            children: [
                              Container(
                                width: 35,
                                height: 35,
                                decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 224, 241, 255),
                                    shape: BoxShape.circle),
                                child: Icon(
                                  Icons.error,
                                  size: 18,
                                  color: Color(0xFF009cff),
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                child: Text(
                                  'Isi informasi basik dibawah ini, agar bisa mencari data yang akurat',
                                  style: TextStyle(
                                      fontFamily: 'calibri',
                                      color: Colors.grey,
                                      fontSize: 15),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Text(
                            'Tugas',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        crossAxisAlignment: WrapCrossAlignment.start,
                        direction: Axis.horizontal,
                        children: List.generate(categories.length, (index) {
                          return GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectedCategoryIndex = index;
                                });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                      color: selectedCategoryIndex == index
                                          ? Color(0xFF009cff)
                                          : Colors.grey.shade100
                                              .withOpacity(0.8),
                                    ),
                                    color: selectedCategoryIndex == index
                                        ? Color.fromARGB(255, 230, 244, 255)
                                        : Colors.grey.shade100.withOpacity(0.8),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      categories[index],
                                      style: TextStyle(
                                        fontFamily: 'calibri',
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        color: selectedCategoryIndex == index
                                            ? Color(0xFF009cff)
                                            : Colors.grey,
                                      ),
                                    ),
                                  ),
                                ),
                              ));
                        }),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Text(
                            'Tanggal',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Wrap(
                            spacing: 8.0,
                            children:
                                List.generate(categoriesDay.length, (index) {
                              return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedCategoryDayIndex = index;
                                    });
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        border: Border.all(
                                          color:
                                              selectedCategoryDayIndex == index
                                                  ? Color(0xFF009cff)
                                                  : Colors.grey.shade100
                                                      .withOpacity(0.8),
                                        ),
                                        color: selectedCategoryDayIndex == index
                                            ? Color.fromARGB(255, 230, 244, 255)
                                            : Colors.grey.shade100
                                                .withOpacity(0.8),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          categoriesDay[index],
                                          style: TextStyle(
                                            fontFamily: 'calibri',
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400,
                                            color: selectedCategoryDayIndex ==
                                                    index
                                                ? Color(0xFF009cff)
                                                : Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ));
                            }),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                RichText(
                                  text: TextSpan(
                                    text: 'Tanggal',
                                    style: TextStyle(
                                      fontFamily: 'calibri',
                                      color: Colors
                                          .black, // Warna teks sebelum tanda bintang
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: '*',
                                        style: TextStyle(
                                          fontFamily: 'calibri',
                                          color: Colors
                                              .red, // Warna teks setelah tanda bintang
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: TextField(
                                    controller: tanggalAwal,
                                    keyboardType: TextInputType.datetime,
                                    decoration: InputDecoration(
                                      suffixIcon: GestureDetector(
                                        onTap: () => selectTanggalAwal(context),
                                        child: Icon(
                                          Icons.calendar_today,
                                          size: 15,
                                        ),
                                      ),
                                      hintText: '${selectedDate.toLocal()}'
                                          .split(' ')[0],
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 15),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey.shade300)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey.shade300)),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey.shade300)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                RichText(
                                  text: TextSpan(
                                    text: 'Sampai',
                                    style: TextStyle(
                                      fontFamily: 'calibri',
                                      color: Colors
                                          .black, // Warna teks sebelum tanda bintang
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: '*',
                                        style: TextStyle(
                                          fontFamily: 'calibri',
                                          color: Colors
                                              .red, // Warna teks setelah tanda bintang
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: TextField(
                                    controller: tanggalSampai,
                                    keyboardType: TextInputType.datetime,
                                    decoration: InputDecoration(
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          selectTanggalSampai(context);
                                        },
                                        child: Icon(
                                          Icons.calendar_today,
                                          size: 15,
                                        ),
                                      ),
                                      hintText: '${selectedDate.toLocal()}'
                                          .split(' ')[0],
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 15),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey.shade300)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey.shade300)),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey.shade300)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Kepemilikan',
                          style: TextStyle(
                              fontFamily: 'calibri',
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Row(
                        children: [
                          Checkbox(
                            splashRadius: 12,
                            tristate: true,
                            activeColor: Color(0xFF009cff),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            value: isChecked,
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            onChanged: (value) {
                              setState(() {
                                isChecked = value ?? false;
                              });
                            },
                          ),
                          Text(
                            'Laporan Saya',
                            style:
                                TextStyle(fontFamily: 'calibri', fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            splashFactory: InkRipple.splashFactory,
                            fixedSize: Size(MediaQuery.of(context).size.width,
                                MediaQuery.of(context).size.height * 0.05),
                            backgroundColor: Color(0xFF009cff),
                          ),
                          onPressed: () {
                            tanggalAwal.clear();
                            tanggalSampai.clear();
                            Navigator.pop(context);
                          },
                          child: Text(
                            'Tampilkan',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          )),
                    )
                  ],
                ),
              ),
            ),
          );
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(color: Color(0xFF009cff)),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 20),
                    child: Row(
                      children: [
                        Expanded(
                          child: TextField(
                            maxLines: 1,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              hintText: 'Search',
                              hintStyle: TextStyle(
                                fontFamily: 'calibri',
                                color: Colors.grey,
                              ),
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.grey,
                              ),
                              fillColor: Colors.white,
                              filled: true,
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey.shade300)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey.shade300)),
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey.shade300)),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Icon(
                            Icons.notifications_outlined,
                            size: 35,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 6,
              child: Container(
                width: double.infinity,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.08,
                        width: MediaQuery.of(context).size.width,
                        child: ListView.builder(
                          itemCount: 1,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                show();
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 15),
                                child: Row(
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height,
                                      width: MediaQuery.of(context).size.width *
                                          0.18,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border: Border.all(
                                              color: Colors.grey.shade300),
                                          color: Colors.white),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Icon(
                                            Icons.filter_alt,
                                            color: Colors.black,
                                            size: 20,
                                          ),
                                          Text(
                                            'Filter',
                                            style: TextStyle(
                                                fontFamily: 'calibri',
                                                color: Colors.black),
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: Color(0xFF009cff)),
                                          color: Color.fromARGB(
                                              255, 230, 244, 255)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          '01 Jan 2021 - 29 Maret 2021',
                                          style: TextStyle(
                                            fontFamily: 'calibri',
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF009cff),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(
                                              color: Color(0xFF009cff)),
                                          color: Color.fromARGB(
                                              255, 230, 244, 255)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          'Minggu Ini',
                                          style: TextStyle(
                                            fontFamily: 'calibri',
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF009cff),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Hasil Pencarian',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Container(
                        width: double.infinity,
                        child: ListView.builder(
                          itemCount: 6,
                          itemBuilder: (context, index) {
                            return Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white,
                                  ),
                                  width: double.infinity,
                                  height:
                                      MediaQuery.of(context).size.height * 0.08,
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                        radius: 20,
                                        backgroundImage: AssetImage(
                                            'assets/images/logoAccount.png'),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              "Brookz Simon",
                                              style: TextStyle(
                                                  fontFamily: 'calibri',
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  'Safrudin',
                                                  style: TextStyle(
                                                      fontFamily: 'calibri',
                                                      color: Colors.grey),
                                                ),
                                                Text(
                                                  '29 Maret 2021 14:30',
                                                  style: TextStyle(
                                                      fontFamily: 'calibri',
                                                      color: Colors.grey),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

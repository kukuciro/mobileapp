import 'package:flutter/material.dart';
import 'package:mobile_app/Page/Authentication/login.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class codeVerify extends StatefulWidget {
  const codeVerify({super.key});

  @override
  State<codeVerify> createState() => _codeVerifyState();
}

class _codeVerifyState extends State<codeVerify> {
  late List<FocusNode> _focusNodes;
  late List<TextEditingController> _controllers;

  @override
  void initState() {
    super.initState();

    StatusBarControl.setColor(Colors.white);
    StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
    _focusNodes = List.generate(4, (index) => FocusNode());
    _controllers = List.generate(
      4,
      (index) => TextEditingController(),
    );

    for (int i = 0; i < 3; i++) {
      _controllers[i].addListener(() {
        if (_controllers[i].text.isNotEmpty) {
          _focusNodes[i + 1].requestFocus();
        }
      });
    }

    _controllers[3].addListener(() {
      if (_controllers[3].text.isNotEmpty) {
        // Implementasi: Proses atau verifikasi PIN di sini
        // Anda dapat mengakses nilai PIN menggunakan _controllers[index].text
        print(
            'Code Verify: ${_controllers[0].text}${_controllers[1].text}${_controllers[2].text}${_controllers[3].text}');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.black,
            ),
            tooltip: 'back',
            splashRadius: MediaQuery.of(context).size.width * 0.05,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              child: GestureDetector(
                onTap: () {},
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.2,
                  height: MediaQuery.of(context).size.height * 0.2,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Color.fromARGB(255, 202, 231, 255)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.help,
                        color: Color(0xFF009cff),
                        size: 18,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        'Bantuan',
                        style: TextStyle(color: Color(0xFF009cff)),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        body: Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage('assets/images/kodeVerify.png'),
                  height: MediaQuery.of(context).size.height * 0.35,
                  width: MediaQuery.of(context).size.width * 0.45,
                ),
                Text(
                  'Kode Verifikasi',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.22),
                  child: Text(
                    'Silahkan input token untuk bergabung dengan workspace',
                    textAlign: TextAlign.center,
                    style: TextStyle(height: 1.5),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      for (int i = 0; i < 4; i++)
                        Container(
                          width: 50,
                          margin: EdgeInsets.symmetric(horizontal: 8),
                          child: TextField(
                            controller: _controllers[i],
                            focusNode: _focusNodes[i],
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            maxLength: 1, // Untuk menyembunyikan karakter input
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.symmetric(vertical: 10),
                              counterText: '', // Menghapus teks hitungan bawaan
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            splashFactory: InkRipple.splashFactory,
                            fixedSize: Size(MediaQuery.of(context).size.width,
                                MediaQuery.of(context).size.height * 0.066),
                            backgroundColor: Color(0xFF009cff),
                          ),
                          onPressed: () {},
                          child: Text(
                            'Lanjut',
                            style:
                                TextStyle(fontFamily: 'calibri', fontSize: 16),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Sudah punya akun? ',
                            style: TextStyle(
                                fontFamily: 'calibri',
                                fontWeight: FontWeight.w500),
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                PageRouteBuilder(
                                  pageBuilder:
                                      (context, animation, secondaryAnimation) {
                                    return login();
                                  },
                                  transitionsBuilder: (context, animation,
                                      secondaryAnimation, child) {
                                    const begin = Offset(1.0, 0.0);
                                    const end = Offset.zero;
                                    const curve = Curves.easeInOut;

                                    var tween = Tween(begin: begin, end: end)
                                        .chain(CurveTween(curve: curve));

                                    var offsetAnimation =
                                        animation.drive(tween);

                                    return SlideTransition(
                                      position: offsetAnimation,
                                      child: child,
                                    );
                                  },
                                  transitionDuration:
                                      Duration(milliseconds: 500),
                                ),
                              );
                            },
                            child: Text(
                              'Masuk',
                              style: TextStyle(
                                  fontFamily: 'calibri',
                                  color: Color(0xFF009cff),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ],
                      ),
                      SizedBox()
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

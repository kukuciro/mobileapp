import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class forgotPassword extends StatefulWidget {
  const forgotPassword({super.key});

  @override
  State<forgotPassword> createState() => _forgotPasswordState();
}

class _forgotPasswordState extends State<forgotPassword> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.black,
            ),
            tooltip: 'back',
            splashRadius: MediaQuery.of(context).size.width * 0.05,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
              child: GestureDetector(
                onTap: () {},
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.2,
                  height: MediaQuery.of(context).size.height * 0.2,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Color.fromARGB(255, 202, 231, 255)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.help,
                        color: Color(0xFF009cff),
                        size: 18,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        'Bantuan',
                        style: TextStyle(color: Color(0xFF009cff)),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        body: Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage('assets/images/lupaSandi.png'),
                  height: MediaQuery.of(context).size.height * 0.3,
                  width: MediaQuery.of(context).size.width * 0.6,
                ),
                Text(
                  'Lupa Sandi?',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.22),
                  child: Text(
                    'Kode Konfirmasi akan dikirimkan ke alamat emailmu',
                    textAlign: TextAlign.center,
                    style: TextStyle(height: 1.5),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30, bottom: 20),
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                      label: Text(
                        'Email',
                        style: TextStyle(
                          fontFamily: 'calibri',
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300)),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey.shade300)),
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            splashFactory: InkRipple.splashFactory,
                            fixedSize: Size(MediaQuery.of(context).size.width,
                                MediaQuery.of(context).size.height * 0.066),
                            backgroundColor: Color(0xFF009cff),
                          ),
                          onPressed: () {},
                          child: Text(
                            'Kirim',
                            style:
                                TextStyle(fontFamily: 'calibri', fontSize: 16),
                          )),
                      Text(
                        'Kembali ke Masuk',
                        style: TextStyle(
                            fontFamily: 'calibri',
                            color: Color(0xFF009cff),
                            fontWeight: FontWeight.w500),
                      ),
                      SizedBox()
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

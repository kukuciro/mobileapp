import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:status_bar_control/status_bar_control.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class setNewPassword extends StatefulWidget {
  const setNewPassword({super.key});

  @override
  State<setNewPassword> createState() => _setNewPasswordState();
}

class _setNewPasswordState extends State<setNewPassword> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_new_rounded,
              color: Colors.black,
            ),
            tooltip: 'back',
            splashRadius: MediaQuery.of(context).size.width * 0.05,
            onPressed: () {},
          ),
         ),
        body: Container(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: [
                Spacer(),
                Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Atur Ulang Sandi',
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width * 0.18),
                        child: Text(
                          'Sandi baru kamu harus berbeda dari sandi sebelumnya yang anda gunakan',
                          textAlign: TextAlign.center,
                          style: TextStyle(height: 1.5),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 30, bottom: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                            label: Text(
                              'Sandi',
                              style: TextStyle(
                                fontFamily: 'calibri',
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade300)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade300)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade300)),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only( bottom: 20),
                        child: TextField(
                          decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                            label: Text(
                              'Ulang Sandi',
                              style: TextStyle(
                                fontFamily: 'calibri',
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade300)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade300)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey.shade300)),
                          ),
                        ),
                      ),
                      ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  elevation: 0,
                                  splashFactory: InkRipple.splashFactory,
                                  fixedSize: Size(MediaQuery.of(context).size.width,
                                      MediaQuery.of(context).size.height * 0.066),
                                  backgroundColor: Color(0xFF009cff),
                                ),
                                onPressed: () {},
                                child: Text(
                                  'Kirim',
                                  style:
                                      TextStyle(fontFamily: 'calibri', fontSize: 16),
                                )),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

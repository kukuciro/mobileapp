class Weather {
  final double temperature;
  final String mainCondition;
  final double windSpeed;
  final double humadity;
  final double tempMax;
  final double tempMin;
  final String city;
  final String desc;
  final double feels_like;
  final double pressure;
  final double clouds;
  final String country;
  Weather({
    required this.country,
    required this.clouds,
    required this.pressure,
    required this.tempMin,
    required this.tempMax,
    required this.feels_like,
    required this.temperature,
    required this.city,
    required this.mainCondition,
    required this.windSpeed,
    required this.humadity,
    required this.desc,
  });

  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
      city: json['name'],
      desc: json['weather'][0]['description'],
      temperature: json['main']['temp'].toDouble(),
      mainCondition: json['weather'][0]['main'],
      windSpeed: json['wind']['speed'].toDouble(),
      humadity: json['main']['humidity'].toDouble(),
      feels_like: json['main']['feels_like'].toDouble(), 
      clouds: json['clouds']['all'].toDouble(), 
      country: json['sys']['country'], 
      pressure: json['main']['pressure'].toDouble(), 
      tempMax: json['main']['temp_max'].toDouble(), 
      tempMin: json['main']['temp_min'].toDouble(),
    );
  }
}

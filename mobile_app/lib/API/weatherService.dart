import 'dart:convert';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'weatherModel.dart';
import 'package:geocoding/geocoding.dart';

class weatherService {
  static const BaseUrl = 'https://api.openweathermap.org/data/2.5/weather';
  final String apiKey;

  weatherService(this.apiKey);

  Future<Weather> getCurrentWeather(String cityName) async {
    final response = await http.get(Uri.parse("$BaseUrl?q=$cityName&appid=$apiKey&units=metric"));

    if (response.statusCode == 200) {
      return Weather.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load weather data: ${response.statusCode}');
    }
  }

  Future<String> getCurrentCity() async {
    LocationPermission permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }

    if (permission == LocationPermission.deniedForever) {
      throw Exception('Location permission permanently denied');
    }

    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    List<Placemark> placemarks = await placemarkFromCoordinates(position.latitude, position.longitude);

    if (placemarks.isEmpty) {
      throw Exception('Failed to retrieve placemarks');
    }
    
    String? city = placemarks[0].locality;
    
    if (city == null) {
      throw Exception('City not found in placemarks');
    }

    return city;
  }
}

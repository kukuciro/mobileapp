import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mobile_app/API/weatherModel.dart';
import 'package:mobile_app/API/weatherService.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
  final _weatherService = weatherService('5b5ee4d3630553b72e8fa514b00e2c34');
  Weather? _weather;

  String cityName = "";

  Future<void> _fetchWeather(String cityNames) async {
    try {
      final weather = await _weatherService.getCurrentWeather(cityNames);
      setState(() {
        _weather = weather;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    getCurrentCity().then((_) {
      _fetchWeather(cityName);
    });
  }

  Future<void> getCurrentCity() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }

    if (permission == LocationPermission.deniedForever) {
      setState(() {
        cityName = 'Permission denied';
      });
      return;
    }

    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);

    if (placemarks.isNotEmpty) {
      String? city = placemarks[0].locality;
      if (city != null) {
        if (city.toLowerCase().contains("kecamatan")) {
          List<String> cityParts = city.split(" ");
          if (cityParts.length > 1) {
            city = cityParts[1];
          }
        } else if (city.toLowerCase().contains("kabupaten")) {
          // Jika nama kota mengandung kata "Kabupaten"
          city =
              city.replaceAll("Kabupaten", "").trim(); // Hapus kata "Kabupaten"
        } else {
          // Tambahkan kondisi untuk nama kota (misalnya, "Kota Malang")
          city = city.replaceAll("Kota", "").trim(); // Hapus kata "Kota"
        }
      }
      setState(() {
        cityName = city ?? 'Unknown';
      });
    } else {
      setState(() {
        cityName = 'Unknown';
      });
    }
  }

  String getWeatherImageUrl(String condition) {
    switch (condition.toLowerCase()) {
      case 'thunderstorm':
        return 'assets/images/thunderstorm.jpg';
      case 'clouds':
        return 'assets/images/berawan.jpg';
      case 'rain':
        return 'assets/images/awanhujan.jpg';
      case 'snow':
        return 'assets/images/snowflake.webp';
      default:
        return 'assets/images/white.jpeg'; // Gambar default jika kondisi cuaca tidak ditemukan
    }
  }

  double heightX(BuildContext context, double heights) {
    return MediaQuery.of(context).size.height * heights;
  }

  Icon getWeatherIcon(String condition) {
    switch (condition.toLowerCase()) {
      case 'thunderstorm':
        return Icon(FontAwesomeIcons.thunderstorm,
            color: Color.fromARGB(255, 162, 158, 158),
            size: heightX(context, 0.2));
      case 'clouds':
        return Icon(FontAwesomeIcons.cloudSun,
            color: Color.fromARGB(255, 242, 255, 59),
            size: heightX(context, 0.2));
      case 'rain':
        return Icon(FontAwesomeIcons.cloudShowersHeavy,
            color: Color.fromARGB(255, 34, 211, 255),
            size: heightX(context, 0.2));
      case 'snow':
        return Icon(FontAwesomeIcons.snowflake,
            color: Colors.white, size: heightX(context, 0.2));
      default:
        return Icon(Icons.cloud,
            color: Colors.white, size: heightX(context, 0.2));
    }
  }

  late Icon weatherIcon = getWeatherIcon(_weather?.mainCondition ?? 'unknown');
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    double widht = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
          body: RefreshIndicator(
        onRefresh: () async {
          await Future.delayed(const Duration(seconds: 1));
          setState(() {
            getCurrentCity().then((_) {
              _fetchWeather(cityName);
            });
          });
        },
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              backgroundColor: Colors.blue.shade600,
              pinned: true,
              elevation: 0,
              floating: true,
              expandedHeight: height * 0.4,
              flexibleSpace: FlexibleSpaceBar(
                background: Image.asset(
                  '${getWeatherImageUrl('${_weather?.mainCondition}')}',
                  fit: BoxFit.cover,
                ),
                title: Text(
                  '$cityName',
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            SliverAnimatedOpacity(
              opacity: 1,
              curve: Curves.easeIn,
              duration: Duration(seconds: 1),
              sliver: SliverToBoxAdapter(
                child: Container(
                  width: double.infinity,
                  height: height * 1,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                        Color.fromARGB(255, 26, 83, 255),
                        Color.fromARGB(255, 10, 145, 255).withOpacity(1),
                        Colors.blueAccent.shade100,
                        Colors.blue.shade200,
                      ])),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          top: height * 0.05,
                        ),
                        child: Column(
                          children: [
                            getWeatherIcon('${_weather?.mainCondition}'),
                            SizedBox(
                              height: 20,
                            ),
                            Column(
                              children: [
                                Text(
                                  '${_weather?.temperature.round() ?? ''}°C',
                                  style: TextStyle(
                                      color: Color.fromARGB(255, 255, 255, 255),
                                      fontSize: 55),
                                ),
                                Text(
                                  '${_weather?.mainCondition ?? ''}  , ${_weather?.desc ?? ''}',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: height * 0.1,
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: widht * 0.06),
                          child: Row(
                            children: [
                              Text(
                                'Hari ini',
                                style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: widht * 0.85,
                        height: height * 0.16,
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 86, 179, 255),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.white, offset: Offset(-2, 0)),
                            ],
                            borderRadius: BorderRadius.circular(15)),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Temp Max',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.tempMax.round() ?? ''}°C',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Temp Min',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.tempMin.round() ?? ''}°C',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Kelembapan',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.humadity.round() ?? ''} %',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Tekanan',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.pressure.round() ?? ''} mb',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: widht * 0.85,
                        height: height * 0.16,
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 86, 179, 255),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.white, offset: Offset(-2, 0)),
                            ],
                            borderRadius: BorderRadius.circular(15)),
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Kecepatan Angin',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.windSpeed.round() ?? ''} m/s',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Terasa',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.feels_like.round() ?? ''}°C',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Awan',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.clouds.round() ?? ''} %',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                    'Negara',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: widht * 0.035,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '${_weather?.country ?? ''}',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
